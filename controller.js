'use strict';

var response = require('./res');
var connection = require('./conn');
var bcrypt = require('bcryptjs');

exports.users = function(req, res) {
    connection.query('SELECT * FROM users', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.findUsers = function(req, res) {
    
    var id = req.params.id;

    connection.query('SELECT * FROM users where id = ?',
    [ id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.createUsers = function(req, res) {
    
    var name = req.body.name;
    var username = req.body.username;
    var password = bcrypt.hashSync(req.body.password, 8);
    var application_id = req.body.application_id;

    connection.query('INSERT INTO users (name, username, password, application_id) values (?,?,?,?)',
    [ name, username, password, application_id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Berhasil menambahkan user!", res)
        }
    });
};

exports.updateUsers = function(req, res) {
    
    var id = req.body.id;
    var name = req.body.name;
    var username = req.body.username;

    connection.query('UPDATE users SET name = ?, username = ? WHERE id = 1',
    [ name, username, id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Berhasil merubah user!", res)
        }
    });
};

exports.deleteUsers = function(req, res) {
    
    var id = req.body.id;

    connection.query('DELETE FROM users WHERE id = ?',
    [ id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Berhasil menghapus user!", res)
        }
    });
};

exports.index = function(req, res) {
    response.ok("Hello from the Node JS RESTful side!", res)
};
